(function ($) {

  function escapeHtml(unsafe) {
  return unsafe
      .replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");
  }

  Drupal.behaviors.sourcePopup = {
    attach: function(context, settings) {
      var i,
          classes_array = [],
          active_modifiers;
      for (i = 0; i < settings.sourcePopup.classes.length; i++) {
        classes_array.push('.' + settings.sourcePopup.classes[i]);
      }
      
      function check_keys() {
        return !settings.sourcePopup.modifiers.length || active_modifiers;
      }
      function _get_modifiers() {
        return settings.sourcePopup.modifiers.join("+");
      }

      var sppClickHandlerProvider = function(element, c) {
        return function(e) {
          if (check_keys()) {
            var that = element;
            //console.log($(element).clone().contents());
            $(element).clone().contents().wrapAll('<div class="spp-wrapper"/>').parent('.spp-wrapper').dialog({
              dialogClass: 'spp-dialog',
              title: 'CSS class: ' + c,
              autoOpen: true,
              height: 350, 
              width: '70%',
              open: function(event, ui) {
                $(that).addClass('spp-display');
                $(event.target).html(escapeHtml($(event.target).html()));
                $(event.target).wrapInner('<div class="spp"/>')
                  .children('.spp')
                  .wrapInner('<textarea class="spp-code"/>');
              },
              close: function(event, ui) {
                $(that).removeClass('spp-display');
              }
            });
            e.stopPropagation();
            e.preventDefault();
          }
        } 
      }
  
      $(context)
        .bind("mousemove", (function(classes) {
          var lastElement, lastThing;
          
          function configure_element(elem, aclass) {
            // Remove .spp-hovers from everything else (which should be at maximum one element)
            if (lastThing != undefined) {
              $(lastThing).removeClass('spp-hover');
            }
            // Set .spp-hover to this element 
            $(elem).addClass('spp-hover');
            // Register click handler if none is set
            if (($(elem).data('events') || {})['click'] == undefined) {
              $(elem).bind("click", sppClickHandlerProvider(elem, aclass));
            }
          }
          
          return function (e) {
            var i, elem, currentThing;
            if (check_keys()) {
              // We've just passed borders of e.target 
              if (lastElement != e.target) {
                // Firstly check the classes of e.target
                for (i = 0; i < classes.length; i++) {
                  if (classes[i] && $(e.target).hasClass(classes[i])) {
                    configure_element(e.target, classes[i])
                    currentThing = e.target;
                    // Exiting on first matched class
                    break;
                  }
                }
                // Check if we found something
                if (currentThing == undefined) {
                  // No, continue our search to the parents
                  // Find closest ancestor with any class
                  elem = $(e.target).closest(classes_array.join(',')).get(0);
                  if (elem != undefined) {
                    configure_element(elem, classes[i])
                    currentThing = elem;
                  }
                }
                // Check again if we finally found something
                if (currentThing != undefined) {
                  // Keep the current element
                  lastThing = currentThing; 
                } else {
                  // Remove spp-hover from lastThing
                  if (lastThing != undefined) {
                    $(lastThing).removeClass('spp-hover');
                  }
                }
                lastElement = e.target;
              }
            }
          }
        })(settings.sourcePopup.classes));
        
        if (_get_modifiers()) {
          $(context)
            .bind("keydown", _get_modifiers(), function (e) {
              active_modifiers = _get_modifiers();
              $(classes_array.join(',')).addClass('spp-enabled');
              //$('body').css('cursor', 'help');
            })
            
            .bind("keyup", _get_modifiers(), function (e) {
              active_modifiers = undefined
              $(classes_array.join(',')).removeClass('spp-enabled');
              $('.spp-hover').removeClass('spp-hover');
              //$('body').css('cursor', 'default');
            });
        }
    }
  }

})(jQuery);
